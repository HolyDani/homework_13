#include <iostream>
#include "Helpers.h"
using namespace std;

int main()
{
	int a, b;
	cout << "Enter first num: ";
	cin >> a;
	cout << "Enter second num: ";
	cin >> b;

	cout << "The square of the sum of numbers " << a << " and " << b << " = " << sqrt(a, b);

	return 0;
}